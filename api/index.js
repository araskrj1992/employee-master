const express = require('express');
const router = express.Router();

router.post('/createSlip', (req, res) => {
    const grossIncome = Math.round(req.body.annualSalary / 12);
    let incomeTax = 0
    if (req.body.annualSalary > 0 && req.body.annualSalary <= 18200) {
        incomeTax = 0;
    } else if (req.body.annualSalary > 18200 && req.body.annualSalary <= 37000) {
        incomeTax = (req.body.annualSalary - 18200) * 0.19;
    } else if (req.body.annualSalary > 37000 && req.body.annualSalary <= 87000) {
        incomeTax = 3572 + (req.body.annualSalary - 37000) * 0.325;
    } else if (req.body.annualSalary > 87000 && req.body.annualSalary <= 180000) {
        incomeTax = 19822 + (req.body.annualSalary - 87000) * 0.37;
    } else {
        incomeTax = 54232 + (req.body.annualSalary - 180000) * .45;
    }
    incomeTax = Math.round(incomeTax / 12);
    const netIncome = Math.round(grossIncome - incomeTax)
    const superRate = Math.round(grossIncome * (req.body.superRate / 100));

    res.json({
        data: {
            ...req.body,
            grossIncome: grossIncome,
            incomeTax: incomeTax,
            netIncome: netIncome,
            superRate: superRate
        },
        status: 200
    })

});


module.exports = router;