## Prerequisites

Node Js installed on machine and nodemon install globally

## Commands to run the project

To deploy:

1. run npm install on root folder
2. run nodemon index.js to run server code
3. cd client & run npm install
4. run npm start to run client side

Test Input -

1. First name:"Ankit"
   Last name:"Gupta"
   Super Rate : 9,
   Annual Salary : 60050


2. First name:"Ayush"
   Last name:"Mittal"
   Super Rate : 10,
   Annual Salary : 120000


Test Output


                                          Gross   Income-tax  Net     Super
1. Ankit Gupta ---  1 March - 31 March --- 5004 ---  922  --- 4082 --- 450
2. Ayush Mittal --- 1 April - 30 April --- 10000 --- 2669 --- 7331 --- 1000



