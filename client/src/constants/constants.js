const actionType = {
    CREATESLIP_REQUEST: "createslip request",
    CREATESLIP_SUCCESS: "createslip success",
    CLOSE_SLIPDETAILS:"close slip details popup"
}

export default actionType
