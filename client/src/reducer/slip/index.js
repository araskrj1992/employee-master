import actionType from "../../constants/constants";

const slipDetails = (
  state = { loading: false, slipDetailsPopUp: false, slipData: {} },
  action
) => {
  switch (action.type) {
    case actionType.CREATESLIP_REQUEST:
      return {
        ...state,
        loading: true
      };

    case actionType.CREATESLIP_SUCCESS:
      return {
        ...state,
        loading: false,
        slipDetailsPopUp: true,
        slipData: action.data
      };

    case actionType.CLOSE_SLIPDETAILS:
      return {
        ...state,
        slipDetailsPopUp: false
      };

    default:
      return state;
  }
};

export default slipDetails;
