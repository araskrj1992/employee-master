import { combineReducers } from 'redux'
import slipDetails from './slip'

export default combineReducers({
    slipDetails
})
