import React, { Component } from "react";
import {
  Panel,
  Row,
  Col,
  FormGroup,
  FormControl,
  ControlLabel,
  Button,
  Form,
  HelpBlock
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import moment from "moment";
import SlipDetails from "./slipDetails";
import "react-datepicker/dist/react-datepicker.css";
import Joi from 'joi-browser';

const schemaCreateEmployee = Joi.object().keys({
  fname: Joi.string().required(),
  lname: Joi.string().required(),
  superRate: Joi.number()
    .required()
    .min(0)
    .max(12),
  annualSalary: Joi.number()
    .required()
    .min(1)
});
const keyArr =  {
  fname: "First name",
  lname: "Last name",
  superRate: "Super Rate",
  annualSalary: "Annual Salary"
};

export class Home extends Component {
  state = {
    fname: "",
    lname: "",
    superRate: 0,
    annualSalary: 0,
    startDate: moment(),
    error: {
      fname: "",
      lname: "",
      superRate: "",
      annualSalary: ""
    },
  };


  getStartEndDate(startDate) {
    let newDate = new Date(startDate);
    let lastday = new Date(
      newDate.getFullYear(),
      newDate.getMonth() + 1,
      0
    ).getDate();
    let lastMonth = newDate.getMonth();
    return `1/${(lastMonth + 1) }-${lastday}/${(lastMonth + 1)}`;
  }

  handleChange = e => {
    let { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  dateChange = date => {
    this.setState({
      startDate: date
    });
  };

  submit = e => {
    e.preventDefault();
    const {
      fname,
      lname,
      superRate,
      annualSalary,
      startDate
    } = this.state;
    let data = {
      fname: fname,
      lname: lname,
      superRate: Number(superRate),
      annualSalary: Number(annualSalary),
      startDate: this.getStartEndDate(startDate)
    };
    const errorData = Joi.validate(data, schemaCreateEmployee, {
      abortEarly: false,
      allowUnknown: true
    });

    if (errorData.error != null) {
      let err = errorData.error.details;
      let error = {};
      err.forEach(({message, context}) => {
        let res = message.replace(
          context["key"],
            keyArr[context["key"]]
        );
        error[context["key"]] = res;
      });

        this.setState({ error });
    } else {
        this.setState({ error: {} });
        this.props.createSlip(data);
    }
  };

  render() {
    const { slipDetails, closeDetail } = this.props;
    const { slipData, slipDetailsPopUp } = slipDetails;
    const { error, startDate } = this.state;
    const { fname, lname, superRate, annualSalary} = error;

    return (
      <section>
        <Panel
          style={{ maxWidth: "600px", margin: "20px auto" }}
          bsStyle="primary"
        >
          <Panel.Heading>Employee Details</Panel.Heading>
          <Panel.Body>
            <Form onSubmit={this.submit}>
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>First Name</ControlLabel>
                    <FormControl
                      name="fname"
                      type="text"
                      placeholder="First Name"
                      onChange={this.handleChange}
                    />
                    {error.fname && (
                      <HelpBlock style={{ color: '#990409'}}>{fname}</HelpBlock>
                    )}
                  </FormGroup>
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>Last Name</ControlLabel>
                    <FormControl
                      name="lname"
                      type="text"
                      placeholder="Last Name"
                      onChange={this.handleChange}
                    />
                    {error.lname && (
                      <HelpBlock style={{ color: '#990409'}}>{lname}</HelpBlock>
                    )}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>Super Rate</ControlLabel>
                    <FormControl
                      name="superRate"
                      type="text"
                      placeholder="Super Rate"
                      onChange={this.handleChange}
                    />
                    {error.superRate && (
                      <HelpBlock style={{ color: '#990409'}}>{superRate}</HelpBlock>
                    )}
                  </FormGroup>
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>Annual Salary</ControlLabel>
                    <FormControl
                      name="annualSalary"
                      type="text"
                      placeholder="Annual Salary"
                      onChange={this.handleChange}
                    />
                    {error.annualSalary && (
                      <HelpBlock style={{ color: '#990409'}}>{annualSalary}</HelpBlock>
                    )}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>Start Date</ControlLabel>
                    <DatePicker
                      selected={startDate}
                      onChange={this.dateChange}
                      className="form-control"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <Button type="submit" bsStyle="primary">
                    Submit
                  </Button>
                </Col>
              </Row>
            </Form>
          </Panel.Body>
        </Panel>
        {slipDetailsPopUp && (
          <SlipDetails
            info={slipData}
            handleClose={closeDetail}
          />
        )}
      </section>
    );
  }
}
