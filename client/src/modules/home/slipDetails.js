import React from 'react'
import { Modal, Button,Table } from 'react-bootstrap'


const SlipDetails = ({
    handleClose,
    info
}) => {
    return (
        <Modal show={true} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Pay Slip</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Pay Period</th>
                            <th>Gross Income</th>
                            <th>Income Tax</th>
                            <th>Net Income</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{info.fname + ' '+ info.lname}</td>
                            <td>{info.startDate}</td>
                            <td>{info.grossIncome}</td>
                            <td>{info.incomeTax}</td>
                            <td>{info.netIncome}</td>
                        </tr>
                    </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleClose}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}


export default SlipDetails;
