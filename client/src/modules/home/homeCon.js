import { Home } from './homeCmp';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import actions from './actions';

const mapStateToProps = (state) => (
    {
        slipDetails: state.slipDetails
    }
);

const mapDispatchToProps = (dispatch) => (
    {
        createSlip: (data) => dispatch(actions.createSlip(data)),
        closeDetail: () => dispatch(actions.closeDetail())
    }
);

export default (connect(mapStateToProps, mapDispatchToProps)(Home));
