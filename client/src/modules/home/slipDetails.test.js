import React from "react";
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import SlipDetails from './slipDetails';
import { Modal } from 'react-bootstrap'

Enzyme.configure({ adapter: new Adapter() });

describe('SlipDetails Component', () => {
    const props = {
    handleClose:() => {},
    info: {
        fname: 'first',
        lname: 'last',
        startDate: 'tst',
        grossIncome: 'test',
        incomeTax: '',
        netIncome: 'df',
    }
}
const slip_details = mount(<SlipDetails { ...props } />);


   it('should render "<Modal/>"', () => {
        expect(slip_details.find(Modal).length).toEqual(1);
   });

   it('should have props equals to props', () => {
        expect(slip_details.props()).toEqual(props);
   });

   it('should have fname props equals to "first"', () => {
        expect(slip_details.props().info.fname).toEqual('first');
   });

   it('should have fname props equals to "first"', () => {
        expect(slip_details.props().info.fname).toEqual('first');
   });
});


describe('When props is not passed' , () => {
const props = {
    handleClose:() => {},
    info: {}
}

const slip_details = mount(<SlipDetails { ...props } />);

   it('should have info props equals to empty object', () => {
        expect(Object.keys(slip_details.props().info).length).toEqual(0);
   });

   it('should have method "handleClose" in props', () => {
        expect(typeof (slip_details.props().handleClose)    ).toEqual('function');
   });


});
