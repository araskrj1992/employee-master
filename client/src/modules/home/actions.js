import actionType from "../../constants/constants";
import fetch from "cross-fetch";

const actions = {
  createSlip(data) {
    return async dispatch => {
      dispatch({ type: actionType.CREATESLIP_REQUEST });
      const reqData = JSON.stringify({
        fname: data.fname,
        lname: data.lname,
        superRate: data.superRate,
        annualSalary: data.annualSalary,
        startDate: data.startDate
      });
      try {
        const fetchData = await fetch("/api/createSlip", {
          method: "post",
          body: reqData,
          headers: {
            "content-type": "application/json"
          }
        });
        const { data } = await fetchData.json();
        dispatch({ type: actionType.CREATESLIP_SUCCESS, data });
      } catch (err) {
        console.log(err);
      }
    };
  },
  closeDetail() {
    return { type: actionType.CLOSE_SLIPDETAILS };
  }
};

export default actions;
