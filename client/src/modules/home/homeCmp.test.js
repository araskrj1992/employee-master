import React from "react";
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import { Home } from './homeCmp';

Enzyme.configure({ adapter: new Adapter() });

const props = {
    slipDetails: {
        slipDetailsPopUp: true,
        slipData: {},
    },
    closeDetail:() => {},
}
const home_component = mount(<Home { ...props } />);

describe('Home Component', () => {
  it('should render a "<Form/>"', () => {
      expect(home_component.find('Home').length).toEqual(1);
  });

  it('should render "<FormControl/>"', () => {
      expect(home_component.find('FormControl').length).toEqual(4);
  });

  it('should render "<SlipDetails/>" when state "slipDetailsPopUp" equals to true', () => {
      home_component.setState({slipDetailsPopUp: true});
      expect(home_component.find('SlipDetails').length).toEqual(1);
  });

  it('method "getStartEndDate" will return "1/6-30/6"', () => {
      const start_end_date = home_component.instance().getStartEndDate('06/18/2018');

      expect(start_end_date).toEqual('1/6-30/6');
  });

  it('method "handleChange" will update fname state', () => {
      const event = {
          target: {
              value: 'test',
              name: 'fname',
          }
      };
      home_component.instance().handleChange(event);

      expect(home_component.state().fname).toEqual('test');
  });

  it('method "submit" will update state with errors', () => {
      const event = {
          preventDefault() {}
      };
      const data = {
          fname: 'test',
          lname: '',
          superRate: '',
          annualSalary: 1000,
      };
      home_component.setState({...data});
      home_component.instance().submit(event);

      expect(home_component.state().error).toEqual( { lname: '"Last name" is not allowed to be empty' });
  });
});
