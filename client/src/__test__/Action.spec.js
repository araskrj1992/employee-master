import actions from "../modules/home/actions";
import slipReducer from "../reducer/slip";
import types from "../constants/constants";

describe("actions and reducer", () => {
  it("CLOSE_SLIPDETAILS", () => {
    expect(actions.closeDetail()).toEqual({ type: types.CLOSE_SLIPDETAILS });
  });

  it("should return the initial state", () => {
    expect(slipReducer(undefined, {})).toEqual({
      loading: false,
      slipDetailsPopUp: false,
      slipData: {}
    });
  });

  it("should return the initial state", () => {
    expect(slipReducer(undefined, {})).toEqual({
      loading: false,
      slipDetailsPopUp: false,
      slipData: {}
    });
  });

  it("should handle CREATESLIP_REQUEST", () => {
    expect(
      slipReducer(undefined, {
        type: types.CREATESLIP_REQUEST
      })
    ).toEqual({ loading: true, slipDetailsPopUp: false, slipData: {} });
  });

  it("should handle CREATESLIP_SUCCESS", () => {
    expect(
      slipReducer(undefined, {
        type: types.CREATESLIP_SUCCESS,
        data: {}
      })
    ).toEqual({ loading: false, slipDetailsPopUp: true, slipData: {} });
  });

  it("should handle CLOSE_SLIPDETAILS", () => {
    expect(
      slipReducer(undefined, {
        type: types.CLOSE_SLIPDETAILS
      })
    ).toEqual({ loading: false, slipDetailsPopUp: false, slipData: {} });
  });
});
