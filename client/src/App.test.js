import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import mainStore from "./store/store";
import App from "./App";

describe("check renders", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <Provider store={mainStore}>
        <App />
      </Provider>,
      div
    );
  });
});
