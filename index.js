const express = require('express');
const path = require('path');
var bodyParser = require('body-parser')
const app = express();
var api = require('./api')

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));


app.use(bodyParser.json())


app.use(function (req, res, next) {
  var contentType = req.headers['content-type'];
  if (req.method === 'POST' && (!contentType || contentType.indexOf('application/json') === -1)) {
    return res.send(400);
  }
  next();
});

app.use('/api', api);
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`App listening on ${port}`);
